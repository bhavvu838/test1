package page;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import locators.pagelocators;
import selenium.SafeActions;

public class TestCase extends SafeActions implements pagelocators {
	WebDriver driver;
	public TestCase(WebDriver driver){
		super(driver);
		this.driver=driver;
	}
	public void clickLogin(){
		safeClick(SIGNINBTN,10000);
		switchFrame(fname);
		safeType(USRNAME,"naveentedla@gmail.com",10000);
		safeType(PASSWD,"9948699682",10000);
		safeClick(SIGNIN,10000);
	}
	public void userInfo(){
		safeClick(ONEWAY,5000);
		safeType(SRC,"Hyderabad",2000);
		safeClick(SELSRC,2000);
		safeType(DEST,"Delhi",2000);
		safeClick(SELDEST,2000);
		safeClick(STRTDATE,2000);
		safeClick(SELDATE,2000);
		safeClick(NOOFPSNGRS,2000);
		safeSelectInDropdownByIndex(CHILDRN,2,2000);
		closeDropdown(PAXCLS,2000);
	}
	public void selectClass(){
		safeSelectInDropdownByIndex(SLTCLS,0,2000);
		safeClick(SRCHBTN,2000);
	}
	public void getFlightNames(){
		List<WebElement> fares=driver.findElements(By.xpath(".//*[@id='onwCity']//span[@name='normalfare']"));
		  for(int i=0;i<fares.size();i++){
			  String val=fares.get(i).getText();
			  Map al=new HashMap();
			  String gh=val.replaceAll(",","");
			  List<WebElement> flight=driver.findElements(By.xpath(".//*[@id='onwCity']//div[contains(@class,'flName padL20')]"));	  
				if(Integer.parseInt(gh)>15000){
					for(int j=i;j<=i;j++){
					    String fname=flight.get(j).getText();
					    al.put(fname,gh);
					}
					System.out.println(al);
				}
		  }
	}
//  @Test
//  public void f() {
//  }
}
