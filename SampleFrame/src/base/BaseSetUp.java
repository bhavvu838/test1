package base;


import org.testng.annotations.Test;

import org.testng.annotations.BeforeSuite;

import org.testng.annotations.BeforeTest;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.annotations.AfterSuite;

import org.testng.annotations.AfterTest;


public class BaseSetUp {

	WebDriver driver;

	public WebDriver getDriver(){

		return driver;

	}

	public void setDriver(WebDriver driver){

		 this.driver=driver;

	}

  @BeforeTest
  public void beforeSuite() {

	  System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

		driver=new ChromeDriver();

		driver.manage().window().maximize();

		driver.get("https://www.goibibo.com/");

  }


  @AfterTest
  public void afterSuite() {

	  driver.quit();

  }


}
