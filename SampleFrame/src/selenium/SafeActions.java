package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class SafeActions {
	WebDriver driver;
	public SafeActions(WebDriver driver){
	//	super(driver);
		this.driver=driver;
	}
	public void safeClick(By locator,int waittime){
		if(waitUntilClickable(locator,waittime)){
			driver.findElement(locator).click();
			System.out.println("clicked on the button successfully");
		}
		else{
			System.out.println("element is not clickable in given time");
			Assert.fail("failed to click on the button");
		}
	}
	public void safeType(By locator,String text,int waitTime){
		if(isElementPresent(locator,waitTime)){
			WebElement element=driver.findElement(locator);
			element.click();
			element.sendKeys(text);
		}
	}
	public void safeSelectInDropdownByIndex(By locator,int indexoption,int waitTime){
		if(isElementPresent(locator,waitTime)){
			WebElement element=driver.findElement(locator);
			Select select = new Select(element);
			select.selectByIndex(indexoption);
			System.out.println("selected index value successfully");
		}
		else{
			System.out.println("element is not selectable by index value");
		}
	}
	public void switchFrame(String framename){
		driver.switchTo().frame(framename);
	}
	public void closeDropdown(By locator,int waitTime){
		if(isElementPresent(locator,waitTime)){
			driver.findElement(locator).click();
		}
		else{
			System.out.println("close button is not there");
		}
	}
	public boolean isElementPresent(By locator,int waitTime){
		boolean eflag=false;
		WebDriverWait wait = new WebDriverWait(driver, waitTime);
		wait.until(ExpectedConditions.presenceOfElementLocated(locator)); 			
		if(driver.findElement(locator).isDisplayed()){
			eflag = true;
			System.out.println("element is displayed");
		}
		else{
			eflag=false;
			System.out.print("element is not displayed to enter data");
		}
		return eflag;
	}
	public boolean waitUntilClickable(By locator,int waitTime){
		boolean bflag=false;
		WebDriverWait wait = new WebDriverWait(driver,waitTime);
		wait.until(ExpectedConditions.elementToBeClickable(locator));
		if(driver.findElement(locator).isDisplayed()){
			bflag=true;
			System.out.println("element is displayed and ready to clickable");
		}
		else{
			bflag=false;
			System.out.println("element is not displayed to click in given wait time");
		}
		return bflag;
	}

}
